package com.valid.music.ViewModel;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.valid.music.Aplication.App;
import com.valid.music.Repository.Repository;
import com.valid.music.Repository.Room.Entity.GetTopArtistsResponse.Artist;
import com.valid.music.Repository.Room.Entity.GetTopTracksResponse.Track;

import java.util.List;

public class CardsViewModel extends ViewModel {
    private Repository repository;
    private LiveData<List<Artist>> artistas; //LiveData de lista de artistas
    private LiveData<List<Track>> canciones; //LiveData de lista de canciones
//    ****************************************** GET ***********************************************

    public void swapPrefs(SharedPreferences preferences){
        repository = App.getInstance().getRepository();
        repository.swapPreferences(preferences);
    }

    //Referencia al repositorio
    public Repository getRepository() {
        return repository;
    }

    public void setContext(Context context_){
        com.valid.music.Aplication.App.getInstance().setContext(context_);
    }

    public void getGetTopArtists(int page, Context ctx){
        repository = App.getInstance().getRepository();
        repository.getGetTopArtists(page, ctx);
    }

    public void getGetTopTracks(int page, Context ctx){
        repository = App.getInstance().getRepository();
        repository.getGetTopTracks(page, ctx);
    }

    public void getSearchTopArtists(int page, String search, Context ctx){
        repository = App.getInstance().getRepository();
        repository.getSearchTopArtists(page, search, ctx);
    }

    public void getSearchTopTracks(int page, String search, Context ctx){
        repository = App.getInstance().getRepository();
        repository.getSearchTopTracks(page, search, ctx);
    }

    //Metodos ROOM
    //getExams recive la llamada y los parametros desde la vista para obtener la lista de artistas
    public LiveData<List<Artist>> getArtistas() {
        repository = App.getInstance().getRepository();
        //getPopularMacroCategory realiza un query en Room para obtener los exams
        artistas = repository.getMusicaAppDao().getArtistas();
        return artistas;
    }

    //getExams recive la llamada y los parametros desde la vista para obtener la lista de canciones
    public LiveData<List<Track>> getCanciones() {
        repository = App.getInstance().getRepository();
        //getPopularMacroCategory realiza un query en Room para obtener los exams
        canciones = repository.getMusicaAppDao().getCanciones();
        return canciones;
    }
}
