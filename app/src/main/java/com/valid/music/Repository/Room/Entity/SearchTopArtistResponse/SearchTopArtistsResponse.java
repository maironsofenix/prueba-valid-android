package com.valid.music.Repository.Room.Entity.SearchTopArtistResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchTopArtistsResponse {
    @SerializedName("results")
    @Expose
    public Results results;
}
