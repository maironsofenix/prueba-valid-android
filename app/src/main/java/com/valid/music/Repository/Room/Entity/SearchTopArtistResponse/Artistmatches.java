package com.valid.music.Repository.Room.Entity.SearchTopArtistResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.valid.music.Repository.Room.Entity.GetTopArtistsResponse.Artist;

import java.util.List;

public class Artistmatches {
    @SerializedName("artist")
    @Expose
    public List<Artist> artists = null;
}
