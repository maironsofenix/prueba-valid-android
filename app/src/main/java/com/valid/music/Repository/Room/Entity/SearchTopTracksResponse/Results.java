package com.valid.music.Repository.Room.Entity.SearchTopTracksResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Results {
    @SerializedName("trackmatches")
    @Expose
    public Trackmatches trackmatches;
}
