package com.valid.music.Repository.Room.Entity.GetTopTracksResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Artist {

    @SerializedName("mbid")
    @Expose
    public String mbid;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("url")
    @Expose
    public String url;
}
