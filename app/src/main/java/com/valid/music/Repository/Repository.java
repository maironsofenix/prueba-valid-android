package com.valid.music.Repository;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.otto.Bus;
import com.valid.music.Repository.Api.WebService;
import com.valid.music.Repository.Room.Entity.GetTopArtistsResponse.Artist;
import com.valid.music.Repository.Room.Entity.GetTopArtistsResponse.GetTopArtistsResponse;
import com.valid.music.Repository.Room.Entity.GetTopTracksResponse.GetTopTracksResponse;
import com.valid.music.Repository.Room.Entity.GetTopTracksResponse.Track;
import com.valid.music.Repository.Room.Entity.SearchTopArtistResponse.SearchTopArtistsResponse;
import com.valid.music.Repository.Room.Entity.SearchTopTracksResponse.SearchTopTracksResponse;
import com.valid.music.Utils.ConstantList;
import com.valid.music.Utils.General;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class Repository {

    private WebService webService;
    private Executor executor;
    private SharedPreferences preferences;
    private MusicaAppDao musicaAppDao;
    private Bus bus;

    public boolean isConected = true;
    private Gson gson;

    public Repository(Bus bus, WebService webService, MusicaAppDao musicaAppDao, Executor executor, SharedPreferences preferences) {
        this.webService = webService;
        this.executor = executor;
        this.preferences = preferences;
        this.bus = bus;
        this.musicaAppDao = musicaAppDao;

        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        this.gson = builder.create();
        bus.register(this);

    }

    //Este metodo retorna el bus de evento desde la clase Repository
    public Bus getBus() {
        return bus;
    }

    public void swapPreferences(SharedPreferences preferences){
        this.preferences = preferences;
    }

    public MusicaAppDao getMusicaAppDao() {
        return musicaAppDao;
    }

    public void getGetTopArtists(int page, final Context ctx) {
        Timber.e("GetTopArtistsResponse - page: " + gson.toJson(page));
        Call<GetTopArtistsResponse> listResponse = webService.getGetTopArtists(ConstantList.API_KEY_SERVICE, "json", "spain", "geo.gettopartists", page);

        listResponse.enqueue(new Callback<GetTopArtistsResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetTopArtistsResponse> call, @NonNull final Response<GetTopArtistsResponse> response) {
                if(response.body() != null){
                    if(response.body().topartists != null){
                        executor.execute(new Runnable() {
                            @Override
                            public void run() {
                                if(response.body().topartists.artists != null){
                                    General.saveTopArtists(ctx, response.body());

                                    List<Artist> artistasBD = new ArrayList<>();
                                    for (Artist artist : response.body().topartists.artists) {
                                        artist.image = artist.images.get(4).url;
                                        artistasBD.add(artist);
                                    }
                                    musicaAppDao.insertArtistas(artistasBD);
                                    Timber.e(ConstantList.TOP_ARTISTS_LIST_SUCCESS + ": "+ response.toString());
                                    bus.post(ConstantList.TOP_ARTISTS_LIST_SUCCESS);
                                }else{
                                    General.clearTopArtists(ctx);
                                    Timber.e(ConstantList.TOP_ARTISTS_LIST_FAIL + response.toString());
                                    bus.post(ConstantList.TOP_ARTISTS_LIST_FAIL);
                                }
                            }
                        });
                    }
                } else {
                    General.clearTopArtists(ctx);
                    Timber.e(ConstantList.TOP_ARTISTS_LIST_FAIL + response.toString());
                    bus.post(ConstantList.TOP_ARTISTS_LIST_FAIL);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetTopArtistsResponse> call, Throwable t) {
                General.clearTopArtists(ctx);
                Timber.e(ConstantList.TOP_ARTISTS_LIST_ERROR + t.getMessage());
                bus.post(ConstantList.TOP_ARTISTS_LIST_ERROR);

            }
        });
    }

    public void getGetTopTracks(int page, final Context ctx) {
        Timber.e("GetTopTracksResponse - page: " + gson.toJson(page));
        Call<GetTopTracksResponse> listResponse = webService.getGetTopTracks(ConstantList.API_KEY_SERVICE, "json", "spain", "geo.gettoptracks", page);

        listResponse.enqueue(new Callback<GetTopTracksResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetTopTracksResponse> call, @NonNull final Response<GetTopTracksResponse> response) {
                if(response.body() != null){
                    if(response.body().tracks != null){
                        executor.execute(new Runnable() {
                            @Override
                            public void run() {
                                if(response.body().tracks.tracks != null){
                                    General.saveTopTracks(ctx, response.body());

                                    List<Track> tracksBD = new ArrayList<>();
                                    for (Track track : response.body().tracks.tracks) {
                                        track.image = track.images.get(3).url;
                                        track.artistName = track.artist.name;
                                        tracksBD.add(track);
                                    }
                                    musicaAppDao.insertCanciones(tracksBD);

                                    Timber.e(ConstantList.TOP_TRACKS_LIST_SUCCESS + " " + response.toString());
                                    bus.post(ConstantList.TOP_TRACKS_LIST_SUCCESS);
                                }else{
                                    General.clearTopTracks(ctx);
                                    Timber.e(ConstantList.TOP_TRACKS_LIST_FAIL + response.toString());
                                    bus.post(ConstantList.TOP_TRACKS_LIST_FAIL);
                                }
                            }
                        });
                    }
                } else {
                    General.clearTopTracks(ctx);
                    Timber.e(ConstantList.TOP_TRACKS_LIST_FAIL + response.toString());
                    bus.post(ConstantList.TOP_TRACKS_LIST_FAIL);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetTopTracksResponse> call, Throwable t) {
                General.clearTopTracks(ctx);
                Timber.e(ConstantList.TOP_TRACKS_LIST_ERROR + t.getMessage());
                bus.post(ConstantList.TOP_TRACKS_LIST_ERROR);

            }
        });
    }

    public void getSearchTopTracks(int page, String search, final Context ctx) {
        Timber.e("GetSearchTopTracksResponse - page: " + gson.toJson(page));
        Call<SearchTopTracksResponse> listResponse = webService.getSearchTopTracks(ConstantList.API_KEY_SERVICE, "json", search, "track.search", page);

        listResponse.enqueue(new Callback<SearchTopTracksResponse>() {
            @Override
            public void onResponse(@NonNull Call<SearchTopTracksResponse> call, @NonNull final Response<SearchTopTracksResponse> response) {
                if(response.body() != null){
                    if(response.body().results != null){
                        executor.execute(new Runnable() {
                            @Override
                            public void run() {
                                if(response.body().results.trackmatches != null){
                                    General.saveSearchTopTracks(ctx, response.body());
                                    Timber.e(ConstantList.SEARCH_TOP_TRACKS_LIST_SUCCESS + " " + response.toString());
                                    bus.post(ConstantList.SEARCH_TOP_TRACKS_LIST_SUCCESS);
                                }else{
                                    General.clearSearchTopTracks(ctx);
                                    Timber.e(ConstantList.TOP_TRACKS_LIST_FAIL + response.toString());
                                    bus.post(ConstantList.TOP_TRACKS_LIST_FAIL);
                                }
                            }
                        });
                    }
                } else {
                    General.clearSearchTopTracks(ctx);
                    Timber.e(ConstantList.TOP_TRACKS_LIST_FAIL + response.toString());
                    bus.post(ConstantList.TOP_TRACKS_LIST_FAIL);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SearchTopTracksResponse> call, Throwable t) {
                General.clearSearchTopTracks(ctx);
                Timber.e(ConstantList.TOP_TRACKS_LIST_ERROR + t.getMessage());
                bus.post(ConstantList.TOP_TRACKS_LIST_ERROR);

            }
        });
    }

    public void getSearchTopArtists(int page, String search, final Context ctx) {
        Timber.e("GetSearchTopArtistsResponse - page: " + gson.toJson(page));
        Call<SearchTopArtistsResponse> listResponse = webService.getSearchTopArtists(ConstantList.API_KEY_SERVICE, "json", search, "artist.search", page);

        listResponse.enqueue(new Callback<SearchTopArtistsResponse>() {
            @Override
            public void onResponse(@NonNull Call<SearchTopArtistsResponse> call, @NonNull final Response<SearchTopArtistsResponse> response) {
                if(response.body() != null){
                    if(response.body().results != null){
                        executor.execute(new Runnable() {
                            @Override
                            public void run() {
                                if(response.body().results.artistmatches != null){
                                    General.saveSearchTopArtists(ctx, response.body());
                                    Timber.e(ConstantList.SEARCH_TOP_ARTISTS_LIST_SUCCESS + " " + response.toString());
                                    bus.post(ConstantList.SEARCH_TOP_ARTISTS_LIST_SUCCESS);
                                }else{
                                    General.clearSearchTopArtists(ctx);
                                    Timber.e(ConstantList.TOP_TRACKS_LIST_FAIL + response.toString());
                                    bus.post(ConstantList.TOP_TRACKS_LIST_FAIL);
                                }
                            }
                        });
                    }
                } else {
                    General.clearSearchTopArtists(ctx);
                    Timber.e(ConstantList.TOP_TRACKS_LIST_FAIL + response.toString());
                    bus.post(ConstantList.TOP_TRACKS_LIST_FAIL);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SearchTopArtistsResponse> call, Throwable t) {
                General.clearSearchTopArtists(ctx);
                Timber.e(ConstantList.TOP_TRACKS_LIST_ERROR + t.getMessage());
                bus.post(ConstantList.TOP_TRACKS_LIST_ERROR);

            }
        });
    }

}
