package com.valid.music.Repository;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.valid.music.Repository.Room.Entity.GetTopArtistsResponse.Artist;
import com.valid.music.Repository.Room.Entity.GetTopTracksResponse.Track;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface MusicaAppDao {
    //Inserta todos los artistas dentro de la entidad en Room
    @Insert(onConflict = REPLACE)
    void insertArtistas(List<Artist> artists);

    //Inserta todos las canciones dentro de la entidad en Room
    @Insert(onConflict = REPLACE)
    void insertCanciones(List<Track> tracks);

    //getArtistas selecciona todos los artistas desde Room.
    @Query("SELECT * FROM Artistas")
    LiveData<List<Artist>> getArtistas();

    //getCanciones selecciona todas las canciones desde Room.
    @Query("SELECT * FROM Canciones")
    LiveData<List<Track>> getCanciones();
}
