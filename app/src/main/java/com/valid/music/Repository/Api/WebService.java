package com.valid.music.Repository.Api;

import com.valid.music.Repository.Room.Entity.GetTopArtistsResponse.GetTopArtistsResponse;
import com.valid.music.Repository.Room.Entity.GetTopTracksResponse.GetTopTracksResponse;
import com.valid.music.Repository.Room.Entity.SearchTopArtistResponse.SearchTopArtistsResponse;
import com.valid.music.Repository.Room.Entity.SearchTopTracksResponse.SearchTopTracksResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WebService {
    // --------------------- ASSISTANCE ------------------- //
    @GET("https://ws.audioscrobbler.com/2.0/")
    Call<GetTopArtistsResponse> getGetTopArtists(@Query("api_key") String apiKey, @Query("format") String format, @Query("country") String country, @Query("method") String method, @Query("page") int page);

    @GET("https://ws.audioscrobbler.com/2.0/")
    Call<GetTopTracksResponse> getGetTopTracks(@Query("api_key") String apiKey, @Query("format") String format, @Query("country") String country, @Query("method") String method, @Query("page") int page);

    @GET("https://ws.audioscrobbler.com/2.0/")
    Call<SearchTopArtistsResponse> getSearchTopArtists(@Query("api_key") String apiKey, @Query("format") String format, @Query("artist") String artist, @Query("method") String method, @Query("page") int page);

    @GET("https://ws.audioscrobbler.com/2.0/")
    Call<SearchTopTracksResponse> getSearchTopTracks(@Query("api_key") String apiKey, @Query("format") String format, @Query("track") String track, @Query("method") String method, @Query("page") int page);

}