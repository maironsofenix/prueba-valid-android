package com.valid.music.Repository.Room.Entity.SearchTopTracksResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.valid.music.Repository.Room.Entity.GetTopTracksResponse.Image;

import java.util.List;

public class Track {
    @SerializedName("mbid")
    @Expose
    public String mbid;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("duration")
    @Expose
    public int duration;

    @SerializedName("listeners")
    @Expose
    public int listeners;

    @SerializedName("url")
    @Expose
    public String url;

    @SerializedName("artist")
    @Expose
    public String artist;

    @SerializedName("image")
    @Expose
    public List<Image> images = null;
}
