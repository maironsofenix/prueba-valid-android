package com.valid.music.Repository.Room.Entity.GetTopTracksResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Tracks {
    @SerializedName("track")
    @Expose
    public List<Track> tracks = null;
}
