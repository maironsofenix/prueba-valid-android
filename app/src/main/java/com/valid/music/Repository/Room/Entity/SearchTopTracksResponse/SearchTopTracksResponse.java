package com.valid.music.Repository.Room.Entity.SearchTopTracksResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchTopTracksResponse {
    @SerializedName("results")
    @Expose
    public Results results;
}
