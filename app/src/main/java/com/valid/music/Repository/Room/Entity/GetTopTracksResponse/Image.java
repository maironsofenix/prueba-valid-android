package com.valid.music.Repository.Room.Entity.GetTopTracksResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Image {

    @SerializedName("#text")
    @Expose
    public String url;

    @SerializedName("size")
    @Expose
    public String size;

}
