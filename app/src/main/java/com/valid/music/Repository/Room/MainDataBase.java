package com.valid.music.Repository.Room;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.valid.music.Repository.MusicaAppDao;
import com.valid.music.Repository.Room.Entity.GetTopArtistsResponse.Artist;
import com.valid.music.Repository.Room.Entity.GetTopTracksResponse.Track;

@Database(entities = {
        Artist.class,
        Track.class
}, version = 3, exportSchema = false)



public abstract class MainDataBase extends RoomDatabase {

    public abstract MusicaAppDao musicaAppDao();

}


