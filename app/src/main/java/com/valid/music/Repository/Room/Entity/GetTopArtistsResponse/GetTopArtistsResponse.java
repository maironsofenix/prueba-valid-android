package com.valid.music.Repository.Room.Entity.GetTopArtistsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetTopArtistsResponse {
    @SerializedName("topartists")
    @Expose
    public Topartists topartists;
}
