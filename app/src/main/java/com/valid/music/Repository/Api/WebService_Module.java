package com.valid.music.Repository.Api;

import android.content.Context;

import com.valid.music.Aplication.App_Scope;
import com.valid.music.Aplication.Context_Module;
import com.valid.music.BuildConfig;
import com.valid.music.Utils.ConstantList;

import java.io.File;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

@Module(includes = Context_Module.class)
public class WebService_Module {

    private Retrofit retrofitInstance;

    @App_Scope
    @Provides
    public Retrofit retrofit(OkHttpClient client) {

        if(retrofitInstance == null)
            retrofitInstance = new Retrofit.Builder()
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(ConstantList.SERVIDOR_DEFAULT)
                    .build();
        return retrofitInstance;
    }

    @App_Scope
    @Provides
    public OkHttpClient okHttpClient(HttpLoggingInterceptor loggingInterceptor, Cache cache, Context ctx) {

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .cache(cache);
        //nuevo para ampliar timeOut
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(loggingInterceptor);
        }
        builder.connectTimeout(90, TimeUnit.SECONDS)
                .readTimeout(90, TimeUnit.SECONDS)
                .writeTimeout(90, TimeUnit.SECONDS)
                .cache(cache);
        return builder.build();

    }

    @App_Scope
    @Provides
    public HttpLoggingInterceptor loggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                //JPP - se dejan de publicar los mensajes de intercambio para los WS
                Timber.e(message);
            }
        });
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

    @App_Scope
    @Provides
    public Cache cache(File cacheFile) {
        return new Cache(cacheFile, 10 * 1000 * 1000); // -> 10 MegaBytes Cache
    }

    @App_Scope
    @Provides
    public File cacheFile(Context context) {
        return new File(context.getCacheDir(), "okHttp_Cache");
    }

}

