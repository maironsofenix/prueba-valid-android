package com.valid.music.Repository.Room.Entity.SearchTopTracksResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Trackmatches {
    @SerializedName("track")
    @Expose
    public List<Track> tracks = null;
}
