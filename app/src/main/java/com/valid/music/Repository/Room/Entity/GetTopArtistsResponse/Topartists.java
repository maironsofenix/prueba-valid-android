package com.valid.music.Repository.Room.Entity.GetTopArtistsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.valid.music.Repository.Room.Entity.GetTopTracksResponse.Track;

import java.util.List;

public class Topartists {

    @SerializedName("artist")
    @Expose
    public List<Artist> artists = null;
}
