package com.valid.music.Repository;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.room.Room;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;
import com.valid.music.Aplication.App_Scope;
import com.valid.music.R;
import com.valid.music.Repository.Api.WebService;
import com.valid.music.Repository.Api.WebService_Module;
import com.valid.music.Repository.Room.MainDataBase;

import java.util.concurrent.Executor;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module(includes = {WebService_Module.class})
public class Repository_Module {

    @App_Scope
    @Provides
    public Repository repository(Bus bus, WebService webService, MusicaAppDao musicaAppaDao, Executor executor, SharedPreferences preferences) {
        Repository repository = new Repository(bus, webService, musicaAppaDao, executor, preferences);
        return repository;
    }

    @App_Scope
    @Provides
    public WebService webService(Retrofit retrofitService) {
        return retrofitService.create(WebService.class);
    }

    @App_Scope
    @Provides
    public MusicaAppDao axaColpatriaDao(Context context){
        MainDataBase dataBase = Room.databaseBuilder(context, MainDataBase.class, "MainDataBase").fallbackToDestructiveMigration().allowMainThreadQueries().build();
        return dataBase.musicaAppDao();
    }

    @App_Scope
    @Provides
    public Executor executor()
    {
        Executor executor = new Executor()
        {
            @Override
            public void execute(@NonNull Runnable runnable)
            {
                new Thread(runnable).start();
            }
        };
        return executor;
    }

    @App_Scope
    @Provides
    public SharedPreferences preferences(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(context.getString(R.string.app_name), context.MODE_PRIVATE);
        return preferences;
    }

    //ADAPTER
    @App_Scope
    @Provides
    public Bus bus(){
        return new Bus(ThreadEnforcer.ANY);
    }


}
