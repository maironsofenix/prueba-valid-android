package com.valid.music.Repository.Room.Entity.GetTopTracksResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetTopTracksResponse {
    @SerializedName("tracks")
    @Expose
    public Tracks tracks;
}
