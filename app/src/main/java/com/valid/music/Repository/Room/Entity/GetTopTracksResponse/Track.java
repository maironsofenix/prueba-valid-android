package com.valid.music.Repository.Room.Entity.GetTopTracksResponse;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = "Canciones")
public class Track {

    @PrimaryKey
    @NonNull
    @SerializedName("mbid")
    @Expose
    public String mbid;

    @ColumnInfo(name = "name")
    @SerializedName("name")
    @Expose
    public String name;

    @ColumnInfo(name = "duration")
    @SerializedName("duration")
    @Expose
    public int duration;

    @ColumnInfo(name = "listeners")
    @SerializedName("listeners")
    @Expose
    public int listeners;

    @ColumnInfo(name = "url")
    @SerializedName("url")
    @Expose
    public String url;

    @Ignore
    @ColumnInfo(name = "artist")
    @SerializedName("artist")
    @Expose
    public Artist artist;

    @ColumnInfo(name = "artist")
    @Expose(serialize = false, deserialize = false)
    transient public String artistName;

    @Ignore
    @SerializedName("image")
    @Expose
    public List<Image> images = null;

    @ColumnInfo(name = "image")
    @Expose(serialize = false, deserialize = false)
    transient public String image;
}
