package com.valid.music.Views.Interfaces;

public interface CancionInterface {
    void onCardClick(int index, com.valid.music.Repository.Room.Entity.GetTopTracksResponse.Track selected);
}
