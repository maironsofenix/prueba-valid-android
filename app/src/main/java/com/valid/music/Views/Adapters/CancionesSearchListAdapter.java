package com.valid.music.Views.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.valid.music.R;
import com.valid.music.Repository.Room.Entity.SearchTopTracksResponse.Track;
import com.valid.music.Views.Interfaces.CancionInterface;
import com.valid.music.Views.Interfaces.CancionSearchInterface;

import java.util.ArrayList;
import java.util.List;

public class CancionesSearchListAdapter extends RecyclerView.Adapter<CancionesSearchListAdapter.CardSearchViewHolder> {
    private Context ctx;
    private int layout_item;
    private List<Track> listaSearchCards = new ArrayList<>();
    private CancionInterface callback;
    private CancionSearchInterface callbackSearch;
    private Integer posSeleccion = -1;

    public CancionesSearchListAdapter(Context ctx_, int layout_item_, CancionSearchInterface callback_) {
        this.ctx = ctx_;
        this.layout_item = layout_item_;
        this.callbackSearch = callback_;
    }

    public void swapListaCards(List<com.valid.music.Repository.Room.Entity.SearchTopTracksResponse.Track> listaSearchCards_, int page) {
        if (page == 1)
            this.listaSearchCards.clear();
        this.listaSearchCards.addAll(listaSearchCards_);
        notifyDataSetChanged();
    }

    @Override
    public CancionesSearchListAdapter.CardSearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_cancion, parent, false);
        CancionesSearchListAdapter.CardSearchViewHolder myViewHolder = new CancionesSearchListAdapter.CardSearchViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(CancionesSearchListAdapter.CardSearchViewHolder cardView, int position) {
        com.valid.music.Repository.Room.Entity.SearchTopTracksResponse.Track selOpcion = listaSearchCards.get(position);
        cardView.position = position;
        cardView.seleted = selOpcion;

        cardView.url = selOpcion.images.get(3).url;

        Picasso.with(ctx).load(cardView.url).into(cardView.image);

        cardView.suscriptores.setText(selOpcion.listeners+" Reproducciones");

        cardView.titulo.setText(selOpcion.name+"");
        cardView.artista.setText(selOpcion.artist);
        cardView.mbid= selOpcion.mbid;
    }

    @Override
    public int getItemCount() {
        return listaSearchCards.size();
    }

    class CardSearchViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private CardView card_view;
        private TextView suscriptores;
        private TextView titulo;
        private TextView artista;
        private ImageView image;
        private String url;
        private int position;
        private String mbid;
        private com.valid.music.Repository.Room.Entity.SearchTopTracksResponse.Track seleted;

        CardSearchViewHolder(View cardView) {
            super(cardView);
            card_view = (CardView) cardView.findViewById(R.id.card_panel);
            card_view.setOnClickListener(this);
            suscriptores = (TextView) cardView.findViewById(R.id.num_suscriptores);
            titulo = (TextView) cardView.findViewById(R.id.cardview_tiulo);
            artista = (TextView) cardView.findViewById(R.id.nombre_artista);
            image = (ImageView) cardView.findViewById(R.id.cardview_image);
        }

        @Override
        public void onClick(View v) {
            callbackSearch.onCardClick(position, seleted);
        }
    }
}

