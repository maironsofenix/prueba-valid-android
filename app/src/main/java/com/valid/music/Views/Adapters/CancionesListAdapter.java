package com.valid.music.Views.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.valid.music.R;
import com.valid.music.Repository.Room.Entity.GetTopTracksResponse.Track;
import com.valid.music.Views.Interfaces.CancionInterface;

import java.util.ArrayList;
import java.util.List;

public class CancionesListAdapter extends RecyclerView.Adapter<CancionesListAdapter.CardViewHolder> {
    private Context ctx;
    private int layout_item;
    private List<Track> listaCards = new ArrayList<>();
    private CancionInterface callback;
    private Integer posSeleccion = -1;

    public CancionesListAdapter(Context ctx_, int layout_item_, CancionInterface callback_) {
        this.ctx = ctx_;
        this.layout_item = layout_item_;
        this.callback = callback_;
    }

    public void swapListaCards(List<com.valid.music.Repository.Room.Entity.GetTopTracksResponse.Track> listaCards_, int page) {
        if(page == 1)
            this.listaCards.clear();
        this.listaCards.addAll(listaCards_);
        notifyDataSetChanged();
    }

    @Override
    public CancionesListAdapter.CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_cancion, parent, false);
        CancionesListAdapter.CardViewHolder myViewHolder = new CancionesListAdapter.CardViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(CancionesListAdapter.CardViewHolder cardView, int position) {
        com.valid.music.Repository.Room.Entity.GetTopTracksResponse.Track selOpcion = listaCards.get(position);
        cardView.position = position;
        cardView.seleted = selOpcion;

        if(selOpcion.images != null)
            cardView.url = selOpcion.images.get(3).url;
        else
            cardView.url = selOpcion.image;

        Picasso.with(ctx).load(cardView.url).into(cardView.image);

        cardView.suscriptores.setText(selOpcion.listeners+" Reproducciones");

        cardView.titulo.setText(selOpcion.name+"");

        if(selOpcion.artist != null)
            cardView.artista.setText(selOpcion.artist.name);
        else
            cardView.artista.setText(selOpcion.artistName);

        cardView.mbid= selOpcion.mbid;
    }

    @Override
    public int getItemCount() {
        return listaCards.size();
    }

    class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private CardView card_view;
        private TextView suscriptores;
        private TextView titulo;
        private TextView artista;
        private ImageView image;
        private String url;
        private int position;
        private String mbid;
        private com.valid.music.Repository.Room.Entity.GetTopTracksResponse.Track seleted;

        CardViewHolder(View cardView) {
            super(cardView);
            card_view = (CardView) cardView.findViewById(R.id.card_panel);
            card_view.setOnClickListener(this);
            suscriptores = (TextView) cardView.findViewById(R.id.num_suscriptores);
            titulo = (TextView) cardView.findViewById(R.id.cardview_tiulo);
            artista = (TextView) cardView.findViewById(R.id.nombre_artista);
            image = (ImageView) cardView.findViewById(R.id.cardview_image);
        }

        @Override
        public void onClick(View v) {
            callback.onCardClick(position, seleted);
        }
    }
}
