package com.valid.music.Views.Fragments;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.LifecycleRegistryOwner;

import com.valid.music.Aplication.App;
import com.valid.music.R;
import com.valid.music.Views.Interfaces.OnPushingNewFragmentListener;

public class AcercadeFragment extends Fragment implements LifecycleRegistryOwner {

    private LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    private OnPushingNewFragmentListener onPushingNewFragmentListener;

    public AcercadeFragment() {
    }

    public static AcercadeFragment newInstance() {
        AcercadeFragment fragment = new AcercadeFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_fragment_acerca_de, container, false);
        cargarObjetos(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void cargarObjetos(View view){
    }

    @Override
    public void onResume() {
        super.onResume();
        App.getInstance().getRepository().getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        App.getInstance().getRepository().getBus().unregister(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPushingNewFragmentListener) {
            onPushingNewFragmentListener = (OnPushingNewFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onPushingNewFragmentListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void setOnPushingNewFragmentListener(OnPushingNewFragmentListener onPushingNewFragmentListener) {
        this.onPushingNewFragmentListener = onPushingNewFragmentListener;
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }
}

