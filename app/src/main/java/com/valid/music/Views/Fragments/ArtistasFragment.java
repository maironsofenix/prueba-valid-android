package com.valid.music.Views.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.LifecycleRegistryOwner;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.valid.music.Aplication.App;
import com.valid.music.R;
import com.valid.music.Repository.Room.Entity.GetTopArtistsResponse.Artist;
import com.valid.music.Repository.Room.Entity.GetTopArtistsResponse.GetTopArtistsResponse;
import com.valid.music.Repository.Room.Entity.SearchTopArtistResponse.SearchTopArtistsResponse;
import com.valid.music.Utils.ConstantList;
import com.valid.music.Utils.General;
import com.valid.music.Views.Activities.HomeActivity;
import com.valid.music.Views.Adapters.ArtistasListAdapter;
import com.valid.music.Views.Interfaces.ArtistaInterface;
import com.valid.music.Views.Interfaces.OnPushingNewFragmentListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;

public class ArtistasFragment extends Fragment implements View.OnClickListener, ArtistaInterface, LifecycleRegistryOwner, TextWatcher {

    private GetTopArtistsResponse listaArtistas = null;
    private SearchTopArtistsResponse listaSearchArtistas = null;
    private List<Artist> lista = new ArrayList<>();
    ArtistasListAdapter adArtistas;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView list_artistas;

    private LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    private OnPushingNewFragmentListener onPushingNewFragmentListener;

    private LinearLayout loading;
    private ConstraintLayout layout_artistas;
    private Button button_cargar_mas;
    private EditText buscar_artista;

    int page = 1;

    public ArtistasFragment() {
    }

    public static ArtistasFragment newInstance() {
        ArtistasFragment fragment = new ArtistasFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_fragment_artistas, container, false);
        cargarObjetos(view);
        lanzarConsultaArtista();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @SuppressLint("NewApi")
    public void lanzarConsultaArtista(){
        loading.setVisibility(View.VISIBLE);
        ((HomeActivity) Objects.requireNonNull(getActivity())).lanzarConsultaArtistas(page);
        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(ConstantList.NAME_ACTIVITY_ARTISTAS).commit();
    }

    private void cargarObjetos(View view){
        list_artistas = (RecyclerView) view.findViewById(R.id.lista_artistas);
        loading = (LinearLayout) view.findViewById(R.id.loading);
        loading.setVisibility(View.VISIBLE);
        layout_artistas = (ConstraintLayout) view.findViewById(R.id.layout_artistas);
        layout_artistas.setVisibility(View.GONE);
        button_cargar_mas = (Button) view.findViewById(R.id.button_cargar_mas);
        button_cargar_mas.setOnClickListener(this);
        buscar_artista = (EditText) view.findViewById(R.id.buscar_artista);
        buscar_artista.addTextChangedListener(this);
    }

    public void listarArtistas(){
        listaArtistas = General.getTopArtist(getContext());
        llenarLista((listaArtistas.topartists.artists));
    }

    public void listarArtistasRoom(List<Artist> artistas){
        llenarLista(artistas);
    }

    public void listarSearchArtistas(){
        listaSearchArtistas = General.getSearchTopArtist(getContext());
        llenarLista((listaSearchArtistas.results.artistmatches.artists));
    }

    public void llenarLista(List l){
        adArtistas = new ArtistasListAdapter(getContext(), R.layout.layout_item_card, this);
        adArtistas.swapListaCards(l, page);
        int spanCount=3;
        if(getActivity().getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT)
            spanCount = 2;
        mLayoutManager = new GridLayoutManager(getActivity(), spanCount);
        //list_artistas.addItemDecoration(new GridSpacingItemDecoration(2, 10, true));
        list_artistas.setLayoutManager(mLayoutManager);
        list_artistas.setAdapter(adArtistas);
        list_artistas.setHasFixedSize(true);
    }

    public void hideLoading(){
        loading.setVisibility(View.GONE);
        layout_artistas.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_cargar_mas) {
            loading.setVisibility(View.VISIBLE);
            page++;
            lanzarConsultaArtista();
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onCardClick(int index, Artist selected) {
        ((HomeActivity) Objects.requireNonNull(getActivity())).loadWebView(selected.url, selected.name);
    }

    @Override
    public void onResume() {
        super.onResume();
        App.getInstance().getRepository().getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        App.getInstance().getRepository().getBus().unregister(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPushingNewFragmentListener) {
            onPushingNewFragmentListener = (OnPushingNewFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onPushingNewFragmentListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void setOnPushingNewFragmentListener(OnPushingNewFragmentListener onPushingNewFragmentListener) {
        this.onPushingNewFragmentListener = onPushingNewFragmentListener;
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @SuppressLint("NewApi")
    @Override
    public void afterTextChanged(Editable editable) {
        loading.setVisibility(View.VISIBLE);
        if(!buscar_artista.getText().toString().equals(""))
            ((HomeActivity) Objects.requireNonNull(getActivity())).lanzarConsultaSearchArtistas(page, buscar_artista.getText().toString());
        else
            lanzarConsultaArtista();
    }
}
