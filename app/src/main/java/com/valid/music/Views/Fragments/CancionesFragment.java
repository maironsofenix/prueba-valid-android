package com.valid.music.Views.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.LifecycleRegistryOwner;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.valid.music.Aplication.App;
import com.valid.music.R;
import com.valid.music.Repository.Room.Entity.GetTopTracksResponse.GetTopTracksResponse;
import com.valid.music.Repository.Room.Entity.GetTopTracksResponse.Track;
import com.valid.music.Repository.Room.Entity.SearchTopTracksResponse.SearchTopTracksResponse;
import com.valid.music.Utils.ConstantList;
import com.valid.music.Utils.General;
import com.valid.music.Views.Activities.HomeActivity;
import com.valid.music.Views.Adapters.CancionesListAdapter;
import com.valid.music.Views.Adapters.CancionesSearchListAdapter;
import com.valid.music.Views.Interfaces.CancionInterface;
import com.valid.music.Views.Interfaces.CancionSearchInterface;
import com.valid.music.Views.Interfaces.OnPushingNewFragmentListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;

public class CancionesFragment extends Fragment implements View.OnClickListener, CancionInterface, CancionSearchInterface, LifecycleRegistryOwner, TextWatcher {

    private GetTopTracksResponse listaCanciones = null;
    private SearchTopTracksResponse listaSearchCanciones = null;
    private List<Track> lista = new ArrayList<>();
    CancionesListAdapter adCancion;
    CancionesSearchListAdapter adSearchCancion;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView list_canciones;

    private LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    private OnPushingNewFragmentListener onPushingNewFragmentListener;

    private LinearLayout loading;
    private ConstraintLayout layout_canciones;
    private Button button_cargar_mas;
    private EditText buscar_cancion;

    int page = 1;

    public CancionesFragment() {
    }

    public static CancionesFragment newInstance() {
        CancionesFragment fragment = new CancionesFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_fragment_canciones, container, false);
        cargarObjetos(view);

        lanzarConsultaCanciones();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @SuppressLint("NewApi")
    public void lanzarConsultaCanciones(){
        ((HomeActivity) Objects.requireNonNull(getActivity())).lanzarConsultaCanciones(page);
        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(ConstantList.NAME_ACTIVITY_ARTISTAS).commit();
    }

    private void cargarObjetos(View view){
        list_canciones = (RecyclerView) view.findViewById(R.id.lista_canciones);
        loading = (LinearLayout) view.findViewById(R.id.loading);
        loading.setVisibility(View.VISIBLE);
        layout_canciones = (ConstraintLayout) view.findViewById(R.id.layout_canciones);
        layout_canciones.setVisibility(View.GONE);
        button_cargar_mas = (Button) view.findViewById(R.id.button_cargar_mas);
        button_cargar_mas.setOnClickListener(this);
        buscar_cancion = (EditText) view.findViewById(R.id.buscar_cancion);
        buscar_cancion.addTextChangedListener(this);
    }

    public void listarCanciones(){
        listaCanciones = General.getTopTracks(getContext());
        llenarLista(listaCanciones.tracks.tracks);
    }

    public void listarArtistasRoom(List<Track> tracks){
        llenarLista(tracks);
    }

    public void listarSearchCanciones(){
        listaSearchCanciones = General.getSearchTopTracks(getContext());
        adSearchCancion = new CancionesSearchListAdapter(getContext(), R.layout.layout_item_card, this);
        adSearchCancion.swapListaCards(listaSearchCanciones.results.trackmatches.tracks, page);
        int spanCount=3;
        if(getActivity().getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT)
            spanCount = 2;
        mLayoutManager = new GridLayoutManager(getActivity(), spanCount);
        list_canciones.setLayoutManager(mLayoutManager);
        list_canciones.setAdapter(adSearchCancion);
        list_canciones.setHasFixedSize(true);
    }

    public  void llenarLista(List l){
        adCancion = new CancionesListAdapter(getContext(), R.layout.layout_item_card, this);
        adCancion.swapListaCards(l, page);
        int spanCount=3;
        if(getActivity().getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT)
            spanCount = 2;
        mLayoutManager = new GridLayoutManager(getActivity(), spanCount);
        list_canciones.setLayoutManager(mLayoutManager);
        list_canciones.setAdapter(adCancion);
        list_canciones.setHasFixedSize(true);
    }

    public void hideLoading(){
        loading.setVisibility(View.GONE);
        layout_canciones.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_cargar_mas) {
            loading.setVisibility(View.VISIBLE);
            page++;
            lanzarConsultaCanciones();
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onCardClick(int index, Track selected) {
        ((HomeActivity) Objects.requireNonNull(getActivity())).loadWebView(selected.url, selected.name);
    }

    @Override
    public void onResume() {
        super.onResume();
        App.getInstance().getRepository().getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        App.getInstance().getRepository().getBus().unregister(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPushingNewFragmentListener) {
            onPushingNewFragmentListener = (OnPushingNewFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onPushingNewFragmentListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void setOnPushingNewFragmentListener(OnPushingNewFragmentListener onPushingNewFragmentListener) {
        this.onPushingNewFragmentListener = onPushingNewFragmentListener;
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @SuppressLint("NewApi")
    @Override
    public void afterTextChanged(Editable editable) {
        loading.setVisibility(View.VISIBLE);
        if(!buscar_cancion.getText().toString().equals(""))
            ((HomeActivity) Objects.requireNonNull(getActivity())).lanzarConsultaSearchCanciones(page, buscar_cancion.getText().toString());
        else
            lanzarConsultaCanciones();
    }

    @SuppressLint("NewApi")
    @Override
    public void onCardClick(int index, com.valid.music.Repository.Room.Entity.SearchTopTracksResponse.Track selected) {
        ((HomeActivity) Objects.requireNonNull(getActivity())).loadWebView(selected.url, selected.name);
    }
}
