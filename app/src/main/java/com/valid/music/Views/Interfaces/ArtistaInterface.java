package com.valid.music.Views.Interfaces;

public interface ArtistaInterface {
    void onCardClick(int index, com.valid.music.Repository.Room.Entity.GetTopArtistsResponse.Artist selected);
}
