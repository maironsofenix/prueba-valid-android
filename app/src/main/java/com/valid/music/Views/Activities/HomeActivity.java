package com.valid.music.Views.Activities;

import android.app.Activity;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.squareup.otto.Subscribe;
import com.valid.music.Aplication.App;
import com.valid.music.R;
import com.valid.music.Repository.Room.Entity.GetTopArtistsResponse.Artist;
import com.valid.music.Repository.Room.Entity.GetTopTracksResponse.Track;
import com.valid.music.Utils.ConnectBroadcastReceiver;
import com.valid.music.Utils.ConstantList;
import com.valid.music.Utils.General;
import com.valid.music.ViewModel.CardsViewModel;
import com.valid.music.Views.Fragments.AcercadeFragment;
import com.valid.music.Views.Fragments.ArtistasFragment;
import com.valid.music.Views.Fragments.CancionesFragment;
import com.valid.music.Views.Fragments.MenuNavigationFragment;
import com.valid.music.Views.Fragments.WebViewFragment;
import com.valid.music.Views.Interfaces.OnPushingNewFragmentListener;

import java.util.List;

import timber.log.Timber;

public class HomeActivity extends AppCompatActivity implements OnPushingNewFragmentListener, FragmentManager.OnBackStackChangedListener {

    private ArtistasFragment fragmentArtistas;
    private CancionesFragment fragmentCanciones;
    private MenuNavigationFragment fragmentMenu;
    private AcercadeFragment fragmentAcercaDe;

    //ROOM
    protected LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    protected ConnectBroadcastReceiver receiver = new ConnectBroadcastReceiver();
    protected Boolean isRegistered = false;

    protected CardsViewModel model;

    protected String ultimaURL="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_artistas_activity);

        getSupportActionBar().setTitle(getString(R.string.titulo_toolbar_artistas));

        model = ViewModelProviders.of(this).get(CardsViewModel.class);
        model.setContext(this);

        loadMenuBottom();
        cargarObjetos();
        cargarFragment();
    }

    public void cargarFragment(){
        String current_fragment = General.getCurrentFragment(this);
        switch (current_fragment){
            case ConstantList.FRAGMENT_ACERCADE:
                loadAcercaDe();
                break;
            case ConstantList.FRAGMENT_ARTISTAS:
                loadArtistasFragment();
                break;
            case ConstantList.FRAGMENT_CANCIONES:
                loadCancionesFragment();
                break;
            case ConstantList.FRAGMENT_WEBVIEW:
                loadWebView(General.getCurrentWebViewURL(this), General.getCurrentWebViewTitulo(this));
                break;
        }
    }

    public void cargarObjetos(){
    }

    @Override
    public void onPushNewFragment(Fragment fragment, String tag) {
        if(!tag.equals(ConstantList.FRAGMENT_MENU))
            General.setCurrentFragment(this, tag);

        if(!fragment.isAdded()){
            Log.d(ConstantList.NAME_ACTIVITY_ARTISTAS, fragment.getClass().getCanonicalName());
            switch (tag) {
                case ConstantList.FRAGMENT_ARTISTAS:
                case ConstantList.FRAGMENT_CANCIONES:
                case ConstantList.FRAGMENT_WEBVIEW:
                case ConstantList.FRAGMENT_ACERCADE:
                    getSupportFragmentManager()
                            .beginTransaction()
                            //.remove(fragment)
                            .addToBackStack(tag)
                            .replace(R.id.frame_content, fragment, tag)
                            .commit();
                    break;
                case ConstantList.FRAGMENT_MENU:
                    getSupportFragmentManager()
                            .beginTransaction()
                            //.remove(fragment)
                            .addToBackStack(tag)
                            .replace(R.id.frame_menu, fragment, tag)
                            .commit();
                    break;
            }
        }
    }

    public void lanzarConsultaArtistas(int page){
        if(General.isNetworkAvailable(this)) {
            model.getGetTopArtists(page, this);
        }
            else{
            Timber.e("No tengo internet");
            getArtistasRoom();
        }
    }

    public void lanzarConsultaSearchArtistas(int page, String search){
        if(General.isNetworkAvailable(this)) {
            model.getSearchTopArtists(page, search, this);
        }else{
            Toast toast = Toast.makeText(getBaseContext(), "No tienes conexión a internet", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();
        }
    }

    public void lanzarConsultaCanciones(int page){
        if(General.isNetworkAvailable(this)) {
            model.getGetTopTracks(page, this);
        }
        else{
            Timber.e("No tengo internet");
            getCancionesRoom();
        }
    }

    private void getArtistasRoom() {
        model.getArtistas().observe(this, new Observer<List<Artist>>() {
            @Override
            public void onChanged(@Nullable List<Artist> artistas) {
                Timber.e("Entré OBSERVADOR");
                if (artistas != null && artistas.size() > 0) {
                    fragmentArtistas.listarArtistasRoom(artistas);
                    fragmentArtistas.hideLoading();
                }
                else {
                    Toast toast = Toast.makeText(getBaseContext(), "Debe conectarse al menos una vez a internet para descargar el contenido", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                }
            }
        });
    }

    private void getCancionesRoom() {
        model.getCanciones().observe(this, new Observer<List<Track>>() {
            @Override
            public void onChanged(@Nullable List<Track> canciones) {
                Timber.e("Entré OBSERVADOR");
                if (canciones != null && canciones.size() > 0) {
                    fragmentCanciones.listarArtistasRoom(canciones);
                    fragmentCanciones.hideLoading();
                }
                else {
                    Toast toast = Toast.makeText(getBaseContext(), "Debe conectarse al menos una vez a internet para descargar el contenido", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                }
            }
        });
    }

    public void lanzarConsultaSearchCanciones(int page, String search){
        if(General.isNetworkAvailable(this)) {
            model.getSearchTopTracks(page, search, this);
        }else{
            Toast toast = Toast.makeText(getBaseContext(), "No tienes conexión a internet", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();
        }
    }

    public void loadWebView(String url, String titulo) {
        if(General.isNetworkAvailable(this)) {
            General.setCurrentWebViewURL(this, url);
            General.setCurrentWebViewTitulo(this, titulo);
            WebViewFragment webViewFragment = WebViewFragment.newInstance(url);
            webViewFragment.setOnPushingNewFragmentListener(this);
            getSupportActionBar().setTitle(titulo);
            onPushNewFragment(webViewFragment, ConstantList.FRAGMENT_WEBVIEW);
        }
        else{
            Toast toast = Toast.makeText(getBaseContext(), "No tienes conexión a internet", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();
        }
    }

    public void loadArtistasFragment(){
        fragmentArtistas = fragmentArtistas.newInstance();
        getSupportActionBar().setTitle(getString(R.string.titulo_toolbar_artistas));
        fragmentArtistas.setOnPushingNewFragmentListener(this);
        Timber.e("FUNCION LOAD ARTISTAS");
        onPushNewFragment(fragmentArtistas, ConstantList.FRAGMENT_ARTISTAS);
    }

    public void loadCancionesFragment(){
        fragmentCanciones = fragmentCanciones.newInstance();
        getSupportActionBar().setTitle(getString(R.string.titulo_toolbar_canciones));
        fragmentCanciones.setOnPushingNewFragmentListener(this);
        Timber.e("FUNCION LOAD CANCIONES");
        onPushNewFragment(fragmentCanciones, ConstantList.FRAGMENT_CANCIONES);
    }

    public void loadAcercaDe(){
        fragmentAcercaDe = fragmentAcercaDe.newInstance();
        getSupportActionBar().setTitle(getString(R.string.titulo_toolbar_acerca_de));
        fragmentAcercaDe.setOnPushingNewFragmentListener(this);
        Timber.e("FUNCION LOAD ACERCA DE");
        onPushNewFragment(fragmentAcercaDe, ConstantList.FRAGMENT_ACERCADE);
    }

    public void loadMenuBottom(){
        if(fragmentMenu == null)
            fragmentMenu = fragmentMenu.newInstance(0);
        Timber.e("MENU BOTTOM");
        onPushNewFragment(fragmentMenu, ConstantList.FRAGMENT_MENU);
    }

    @Override
    public void onBackStackChanged() {
        FragmentManager.BackStackEntry backStackEntryAt = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1);
        String currentFragmentNameId = backStackEntryAt.getName();
        Timber.e("Cambio de fragmento: " + currentFragmentNameId);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
            String tagFragment = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
            Timber.e("Atrás -> Tag: " + tagFragment);
            switch (tagFragment) {
                case ConstantList.FRAGMENT_ARTISTAS:
                    getSupportFragmentManager().popBackStackImmediate();
                    break;
                case ConstantList.FRAGMENT_MENU:
                case ConstantList.NAME_ACTIVITY_ARTISTAS:
                case ConstantList.FRAGMENT_ACERCADE:
                    setResult(Activity.RESULT_CANCELED);
                    finish();
                    break;
                case ConstantList.FRAGMENT_WEBVIEW:
                    getSupportFragmentManager().popBackStackImmediate();
                    getSupportFragmentManager().popBackStackImmediate();
                    break;
            }
        }
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isRegistered) {
            App.getInstance().getRepository().getBus().register(this);
            HomeActivity.this.registerReceiver(receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            isRegistered = true;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isRegistered) {
            App.getInstance().getRepository().getBus().unregister(this);
            HomeActivity.this.unregisterReceiver(receiver);
            isRegistered = false;
        }
    }

    @Subscribe
    public void results(final String msj) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (msj) {
                    case ConstantList.TOP_ARTISTS_LIST_SUCCESS:
                        if(fragmentArtistas != null) {
                            fragmentArtistas.listarArtistas();
                            fragmentArtistas.hideLoading();
                        }
                        break;
                    case ConstantList.TOP_TRACKS_LIST_SUCCESS:
                        if(fragmentCanciones != null) {
                            fragmentCanciones.listarCanciones();
                            fragmentCanciones.hideLoading();
                        }
                        break;
                    case ConstantList.SEARCH_TOP_ARTISTS_LIST_SUCCESS:
                        if(fragmentArtistas != null) {
                            fragmentArtistas.listarSearchArtistas();
                            fragmentArtistas.hideLoading();
                        }
                        break;
                    case ConstantList.SEARCH_TOP_TRACKS_LIST_SUCCESS:
                        if(fragmentCanciones != null) {
                            fragmentCanciones.listarSearchCanciones();
                            fragmentCanciones.hideLoading();
                        }
                        break;
                }
            }
        });
    }
}
