package com.valid.music.Views.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.LifecycleRegistryOwner;

import com.valid.music.Aplication.App;
import com.valid.music.R;
import com.valid.music.Views.Interfaces.OnPushingNewFragmentListener;

import java.util.Objects;

import timber.log.Timber;

public class WebViewFragment extends Fragment implements LifecycleRegistryOwner,
        View.OnClickListener {

    WebView webView;
    LinearLayout linear_progress;
    private static final String URL = "url";

    private OnPushingNewFragmentListener onPushingNewFragmentListener;
    private LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);

    private String url;

    public WebViewFragment() {
    }

    public static WebViewFragment newInstance(String url) {
        WebViewFragment fragment = new WebViewFragment();
        Bundle args = new Bundle();
        args.putString(URL, url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.e("ON_CREATE");
        if (getArguments() != null) {
            url = getArguments().getString(URL);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_webview_fragment, container, false);
        webView = view.findViewById(R.id.web_view);
        linear_progress = view.findViewById(R.id.linear_progress);
        goToUrlService();

        if (getArguments() != null) {
            url = getArguments().getString(URL);
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPushingNewFragmentListener) {
            onPushingNewFragmentListener = (OnPushingNewFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        App.getInstance().getRepository().getBus().register(this);

    }

    @SuppressLint("SetJavaScriptEnabled")
    private void goToUrlService() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                linear_progress.setVisibility(View.GONE);
                final String js = "javascript:(function increaseMaxZoomFactor(){ var element = document.reateElement('meta'); element.name = \"viewport\"; element.content = \"maximum-scale=10.0 initial-scale=1\"; var head = document.getElementsByTagName('head')[0]; head.appendChild(element); })()";
                if (Build.VERSION.SDK_INT >= 19) {
                    view.evaluateJavascript(js, new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String s) {

                        }
                    });
                } else {
                    view.loadUrl(js);
                }
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg)
            {
                return true;
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
            }
        });

        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.loadUrl(url);

        webView.setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("NewApi")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() != KeyEvent.ACTION_DOWN)
                    return true;

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (webView.canGoBack()) {
                        webView.goBack();
                    } else {
                        Objects.requireNonNull(getActivity()).onBackPressed();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        App.getInstance().getRepository().getBus().unregister(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onPushingNewFragmentListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void setOnPushingNewFragmentListener(OnPushingNewFragmentListener onPushingNewFragmentListener) {
        this.onPushingNewFragmentListener = onPushingNewFragmentListener;
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }


    @Override
    public void onClick(View view) {
    }

}
