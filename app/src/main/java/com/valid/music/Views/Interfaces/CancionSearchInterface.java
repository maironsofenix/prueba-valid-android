package com.valid.music.Views.Interfaces;

public interface CancionSearchInterface {
    void onCardClick(int index, com.valid.music.Repository.Room.Entity.SearchTopTracksResponse.Track selected);
}
