package com.valid.music.Views.Fragments;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.valid.music.R;
import com.valid.music.Utils.ConstantList;
import com.valid.music.Utils.General;
import com.valid.music.Views.Activities.HomeActivity;

import java.util.Objects;

import timber.log.Timber;

public class MenuNavigationFragment extends Fragment {

    private BottomNavigationView bottomNavigationView;
    private SharedPreferences prefs;
    int menu_activo;

    public MenuNavigationFragment(){
    }

    public static MenuNavigationFragment newInstance(int menu_activo_) {
        MenuNavigationFragment fragment = new MenuNavigationFragment();
        fragment.menu_activo = menu_activo_;
        return fragment;
    }

    public void cargarObjetos(View view){

        bottomNavigationView = (BottomNavigationView) view.findViewById(R.id.menu_bottom);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.item_artistas:
                                lanzarSeccionArtistas();
                                break;
                            case R.id.item_canciones:
                                lanzarSeccionCanciones();
                                break;
                            case R.id.item_acercade:
                                lanzarAcercaDe();
                                break;
                        }
                        return true;
                    }
                });

        cargarFragment();
        Timber.e("Menu activo: "+menu_activo);
    }

    public void cargarFragment(){
        String current_fragment = General.getCurrentFragment(getContext());
        switch (current_fragment){
            case ConstantList.FRAGMENT_ACERCADE:
                bottomNavigationView.setSelectedItemId(R.id.item_acercade);
                break;
            case ConstantList.FRAGMENT_ARTISTAS:
                bottomNavigationView.setSelectedItemId(R.id.item_artistas);
                break;
            case ConstantList.FRAGMENT_CANCIONES:
                bottomNavigationView.setSelectedItemId(R.id.item_canciones);
                break;
            case ConstantList.FRAGMENT_WEBVIEW:
                break;
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_bottom_app_bar, container, false);
        cargarObjetos(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void lanzarSeccionArtistas(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            ((HomeActivity) Objects.requireNonNull(getActivity())).loadArtistasFragment();
        }
    }

    public void lanzarSeccionCanciones(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            ((HomeActivity) Objects.requireNonNull(getActivity())).loadCancionesFragment();
        }
    }

    public void lanzarAcercaDe(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            ((HomeActivity) Objects.requireNonNull(getActivity())).loadAcercaDe();
        }
    }
}
