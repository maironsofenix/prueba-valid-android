package com.valid.music.Views.Interfaces;

import androidx.fragment.app.Fragment;

public interface OnPushingNewFragmentListener {
    void onPushNewFragment(Fragment fragment, String tag);
}
