package com.valid.music.Views.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.valid.music.R;
import com.valid.music.Repository.Room.Entity.GetTopArtistsResponse.Artist;
import com.valid.music.Views.Interfaces.ArtistaInterface;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class ArtistasListAdapter extends RecyclerView.Adapter<ArtistasListAdapter.CardViewHolder> {
    private Context ctx;
    private int layout_item;
    private List<Artist> listaCards = new ArrayList<>();
    private ArtistaInterface callback;
    private Integer posSeleccion = -1;

    public ArtistasListAdapter(Context ctx_, int layout_item_, ArtistaInterface callback_) {
        this.ctx = ctx_;
        this.layout_item = layout_item_;
        this.callback = callback_;
    }

    public void swapListaCards(List<com.valid.music.Repository.Room.Entity.GetTopArtistsResponse.Artist> listaCards_, int page) {
        if(page == 1)
            this.listaCards.clear();
        this.listaCards.addAll(listaCards_);
        notifyDataSetChanged();
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_card, parent, false);
        CardViewHolder myViewHolder = new CardViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(CardViewHolder cardView, int position) {
        com.valid.music.Repository.Room.Entity.GetTopArtistsResponse.Artist selOpcion = listaCards.get(position);
        cardView.position = position;
        cardView.seleted = selOpcion;

        if(selOpcion.images != null)
            cardView.url = selOpcion.images.get(4).url;
        else cardView.url = selOpcion.image;


        if(cardView.url != null && !cardView.url.equals(""))
            Picasso.with(ctx).load(cardView.url).into(cardView.image);
        else
            cardView.image.setImageDrawable(ctx.getResources().getDrawable((R.drawable.imagen_default)));

        cardView.suscriptores.setText(selOpcion.listeners+" Suscriptores");

        cardView.titulo.setText(selOpcion.name+"");
        cardView.mbid= selOpcion.mbid;
    }

    @Override
    public int getItemCount() {
        return listaCards.size();
    }

    class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private CardView card_view;
        private TextView suscriptores;
        private TextView titulo;
        private ImageView image;
        private String url;
        private int position;
        private String mbid;
        private com.valid.music.Repository.Room.Entity.GetTopArtistsResponse.Artist seleted;

        CardViewHolder(View cardView) {
            super(cardView);
            card_view = (CardView) cardView.findViewById(R.id.card_panel);
            card_view.setOnClickListener(this);
            suscriptores = (TextView) cardView.findViewById(R.id.num_suscriptores);
            titulo = (TextView) cardView.findViewById(R.id.cardview_tiulo);
            image = (ImageView) cardView.findViewById(R.id.cardview_image);
        }

        @Override
        public void onClick(View v) {
            Timber.e("Card CLICK");
            callback.onCardClick(position, seleted);
        }
    }
}
