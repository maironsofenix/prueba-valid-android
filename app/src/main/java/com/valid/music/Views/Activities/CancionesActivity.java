package com.valid.music.Views.Activities;

import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleRegistry;

import com.squareup.otto.Subscribe;
import com.valid.music.Aplication.App;
import com.valid.music.R;
import com.valid.music.Utils.ConnectBroadcastReceiver;
import com.valid.music.Utils.ConstantList;
import com.valid.music.Views.Fragments.CancionesFragment;
import com.valid.music.Views.Fragments.MenuNavigationFragment;
import com.valid.music.Views.Interfaces.OnPushingNewFragmentListener;

import timber.log.Timber;

public class CancionesActivity extends AppCompatActivity implements OnPushingNewFragmentListener, FragmentManager.OnBackStackChangedListener {

    private CancionesFragment fragmentCanciones;
    private MenuNavigationFragment fragmentMenu;

    //ROOM
    protected LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    protected ConnectBroadcastReceiver receiver = new ConnectBroadcastReceiver();
    protected Boolean isRegistered = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_canciones_activity);
        cargarObjetos();
        loadCancionesFragment();
        loadMenuBottom();
    }

    public void cargarObjetos(){
    }

    @Override
    public void onPushNewFragment(Fragment fragment, String tag) {
        if(!fragment.isAdded()){
            switch (tag) {
                case ConstantList.FRAGMENT_CANCIONES:
                    getSupportFragmentManager()
                            .beginTransaction()
                            .remove(fragment)
                            .addToBackStack(tag)
                            .add(R.id.frame_content, fragment, tag)
                            .commit();
                    break;
                case ConstantList.FRAGMENT_MENU:
                    getSupportFragmentManager()
                            .beginTransaction()
                            .remove(fragment)
                            .addToBackStack(tag)
                            .add(R.id.frame_menu, fragment, tag)
                            .commit();
                    break;
            }
        }
    }

    public void loadCancionesFragment(){
        if(fragmentCanciones == null)
            fragmentCanciones = fragmentCanciones.newInstance();
        Timber.e("FUNCION LOAD CANCIONES");
        onPushNewFragment(fragmentCanciones, ConstantList.FRAGMENT_CANCIONES);
    }

    public void loadMenuBottom(){
        if(fragmentMenu == null)
            fragmentMenu = fragmentMenu.newInstance(1);
        Timber.e("MENU BOTTOM");
        onPushNewFragment(fragmentMenu, ConstantList.FRAGMENT_MENU);
    }

    @Override
    public void onBackStackChanged() {

    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!isRegistered) {
            App.getInstance().getRepository().getBus().register(this);
            CancionesActivity.this.registerReceiver(receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            isRegistered = true;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isRegistered) {
            App.getInstance().getRepository().getBus().unregister(this);
            CancionesActivity.this.unregisterReceiver(receiver);
            isRegistered = false;
        }
    }

    @Subscribe
    public void results(final String msj) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (msj) {
                    case ConstantList.TOP_TRACKS_LIST_SUCCESS:
                        Timber.e("Entre al subscribe Track");
                        fragmentCanciones.listarCanciones();
                        fragmentCanciones.hideLoading();
                        break;
                }
            }
        });
    }
}
