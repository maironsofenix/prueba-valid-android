package com.valid.music.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.valid.music.Repository.Room.Entity.GetTopArtistsResponse.GetTopArtistsResponse;
import com.valid.music.Repository.Room.Entity.GetTopTracksResponse.GetTopTracksResponse;
import com.valid.music.Repository.Room.Entity.SearchTopArtistResponse.SearchTopArtistsResponse;
import com.valid.music.Repository.Room.Entity.SearchTopTracksResponse.SearchTopTracksResponse;

public class General {

    public static boolean isNetworkAvailable(Context ctx) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void setCurrentFragment(Context ctx, String fragmentName){
        SharedPreferences prefs = ctx.getSharedPreferences(ConstantList.MY_PREFS_APP, Context.MODE_PRIVATE);
        SharedPreferences.Editor PrefEdit = prefs.edit();
        PrefEdit.putString(ConstantList.CURRENT_FRAGMENT, fragmentName);
        PrefEdit.commit();
    }

    public static String getCurrentFragment(Context ctx){
        SharedPreferences prefs = ctx.getSharedPreferences(ConstantList.MY_PREFS_APP, Context.MODE_PRIVATE);
        return prefs.getString(ConstantList.CURRENT_FRAGMENT, ConstantList.FRAGMENT_ARTISTAS);
    }

    public static void setCurrentWebViewURL(Context ctx, String fragmentName){
        SharedPreferences prefs = ctx.getSharedPreferences(ConstantList.MY_PREFS_APP, Context.MODE_PRIVATE);
        SharedPreferences.Editor PrefEdit = prefs.edit();
        PrefEdit.putString(ConstantList.CURRENT_URL_WEBVIEW, fragmentName);
        PrefEdit.commit();
    }

    public static String getCurrentWebViewURL(Context ctx){
        SharedPreferences prefs = ctx.getSharedPreferences(ConstantList.MY_PREFS_APP, Context.MODE_PRIVATE);
        return prefs.getString(ConstantList.CURRENT_URL_WEBVIEW, "");
    }

    public static void setCurrentWebViewTitulo(Context ctx, String fragmentName){
        SharedPreferences prefs = ctx.getSharedPreferences(ConstantList.MY_PREFS_APP, Context.MODE_PRIVATE);
        SharedPreferences.Editor PrefEdit = prefs.edit();
        PrefEdit.putString(ConstantList.CURRENT_TITULO_WEBVIEW, fragmentName);
        PrefEdit.commit();
    }

    public static String getCurrentWebViewTitulo(Context ctx){
        SharedPreferences prefs = ctx.getSharedPreferences(ConstantList.MY_PREFS_APP, Context.MODE_PRIVATE);
        return prefs.getString(ConstantList.CURRENT_TITULO_WEBVIEW, "");
    }

    public static void saveTopTracks(Context ctx, GetTopTracksResponse response){
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gs = builder.create();
        SharedPreferences prefs = ctx.getSharedPreferences(ConstantList.MY_PREFS_APP, Context.MODE_PRIVATE);
        SharedPreferences.Editor PrefEdit = prefs.edit();
        PrefEdit.putString(ConstantList.TOP_TRACKS_LIST, gs.toJson(response));
        PrefEdit.commit();
    }

    public static void saveSearchTopTracks(Context ctx, SearchTopTracksResponse response){
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gs = builder.create();
        SharedPreferences prefs = ctx.getSharedPreferences(ConstantList.MY_PREFS_APP, Context.MODE_PRIVATE);
        SharedPreferences.Editor PrefEdit = prefs.edit();
        PrefEdit.putString(ConstantList.SEARCH_TOP_TRACKS_LIST, gs.toJson(response));
        PrefEdit.commit();
    }

    public static void clearTopTracks(Context ctx){
        SharedPreferences prefs = ctx.getSharedPreferences(ConstantList.MY_PREFS_APP, Context.MODE_PRIVATE);
        SharedPreferences.Editor PrefEdit = prefs.edit();
        PrefEdit.remove(ConstantList.TOP_TRACKS_LIST);
        PrefEdit.commit();
    }

    public static void clearSearchTopTracks(Context ctx){
        SharedPreferences prefs = ctx.getSharedPreferences(ConstantList.MY_PREFS_APP, Context.MODE_PRIVATE);
        SharedPreferences.Editor PrefEdit = prefs.edit();
        PrefEdit.remove(ConstantList.SEARCH_TOP_TRACKS_LIST);
        PrefEdit.commit();
    }

    public static void saveTopArtists(Context ctx, GetTopArtistsResponse response){
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gs = builder.create();
        SharedPreferences prefs = ctx.getSharedPreferences(ConstantList.MY_PREFS_APP, Context.MODE_PRIVATE);
        SharedPreferences.Editor PrefEdit = prefs.edit();
        PrefEdit.putString(ConstantList.TOP_ARTISTS_LIST, gs.toJson(response));
        PrefEdit.commit();
    }

    public static void saveSearchTopArtists(Context ctx, SearchTopArtistsResponse response){
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gs = builder.create();
        SharedPreferences prefs = ctx.getSharedPreferences(ConstantList.MY_PREFS_APP, Context.MODE_PRIVATE);
        SharedPreferences.Editor PrefEdit = prefs.edit();
        PrefEdit.putString(ConstantList.SEARCH_TOP_ARTISTS_LIST, gs.toJson(response));
        PrefEdit.commit();
    }

    public static void clearTopArtists(Context ctx){
        SharedPreferences prefs = ctx.getSharedPreferences(ConstantList.MY_PREFS_APP, Context.MODE_PRIVATE);
        SharedPreferences.Editor PrefEdit = prefs.edit();
        PrefEdit.remove(ConstantList.TOP_ARTISTS_LIST);
        PrefEdit.commit();
    }

    public static void clearSearchTopArtists(Context ctx){
        SharedPreferences prefs = ctx.getSharedPreferences(ConstantList.MY_PREFS_APP, Context.MODE_PRIVATE);
        SharedPreferences.Editor PrefEdit = prefs.edit();
        PrefEdit.remove(ConstantList.SEARCH_TOP_ARTISTS_LIST);
        PrefEdit.commit();
    }

    public static GetTopArtistsResponse getTopArtist(Context ctx){
        GetTopArtistsResponse response = null;
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gs = builder.create();
        SharedPreferences prefs = ctx.getSharedPreferences(ConstantList.MY_PREFS_APP, Context.MODE_PRIVATE);
        String strLista = prefs.getString(ConstantList.TOP_ARTISTS_LIST, "");
        if(!strLista.equals("")){
            response = gs.fromJson(strLista, GetTopArtistsResponse.class);
        }
        return response;
    }

    public static SearchTopArtistsResponse getSearchTopArtist(Context ctx){
        SearchTopArtistsResponse response = null;
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gs = builder.create();
        SharedPreferences prefs = ctx.getSharedPreferences(ConstantList.MY_PREFS_APP, Context.MODE_PRIVATE);
        String strLista = prefs.getString(ConstantList.SEARCH_TOP_ARTISTS_LIST, "");
        if(!strLista.equals("")){
            response = gs.fromJson(strLista, SearchTopArtistsResponse.class);
        }
        return response;
    }

    public static GetTopTracksResponse getTopTracks(Context ctx){
        GetTopTracksResponse response = null;
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gs = builder.create();
        SharedPreferences prefs = ctx.getSharedPreferences(ConstantList.MY_PREFS_APP, Context.MODE_PRIVATE);
        String strLista = prefs.getString(ConstantList.TOP_TRACKS_LIST, "");
        if(!strLista.equals("")){
            response = gs.fromJson(strLista, GetTopTracksResponse.class);
        }
        return response;
    }

    public static SearchTopTracksResponse getSearchTopTracks(Context ctx){
        SearchTopTracksResponse response = null;
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gs = builder.create();
        SharedPreferences prefs = ctx.getSharedPreferences(ConstantList.MY_PREFS_APP, Context.MODE_PRIVATE);
        String strLista = prefs.getString(ConstantList.SEARCH_TOP_TRACKS_LIST, "");
        if(!strLista.equals("")){
            response = gs.fromJson(strLista, SearchTopTracksResponse.class);
        }
        return response;
    }
}
