package com.valid.music.Utils;

public class ConstantList {
    public static final String API_KEY_SERVICE = "829751643419a7128b7ada50de590067";
    public static String SERVIDOR_DEFAULT = "http://ws.audioscrobbler.com";

    public static final String MY_PREFS_APP = "MyPrefsFile";
    public static  final String TOP_TRACKS_LIST = "TOP_TRACKS_LIST";
    public static  final String TOP_TRACKS_LIST_SUCCESS = "TOP_TRACKS_LIST_SUCCESS";
    public static  final String TOP_TRACKS_LIST_ERROR = "TOP_TRACKS_LIST_ERROR";
    public static  final String TOP_TRACKS_LIST_FAIL = "TOP_TRACKS_LIST_FAIL";

    public static  final String SEARCH_TOP_TRACKS_LIST = "SEARCH_TOP_TRACKS_LIST";
    public static  final String SEARCH_TOP_TRACKS_LIST_SUCCESS = "SEARCH_TOP_TRACKS_LIST_SUCCESS";

    public static  final String TOP_ARTISTS_LIST = "TOP_ARTISTS_LIST";
    public static  final String TOP_ARTISTS_LIST_SUCCESS = "TOP_ARTISTS_LIST_SUCCESS";
    public static  final String TOP_ARTISTS_LIST_ERROR = "TOP_ARTISTS_LIST_ERROR";
    public static  final String TOP_ARTISTS_LIST_FAIL = "TOP_ARTISTS_LIST_FAIL";

    public static  final String SEARCH_TOP_ARTISTS_LIST = "SEARCH_TOP_ARTISTS_LIST";
    public static  final String SEARCH_TOP_ARTISTS_LIST_SUCCESS = "SEARCH_TOP_ARTISTS_LIST_SUCCESS";

    public static  final String NAME_ACTIVITY_ARTISTAS = "ACTIVITY_ARTISTAS";

    //Fragmentos
    public static  final String FRAGMENT_ARTISTAS = "FRAGMENT_ARTISTAS";
    public static  final String FRAGMENT_CANCIONES = "FRAGMENT_CANCIONES";
    public static  final String FRAGMENT_MENU = "FRAGMENT_MENU";
    public static  final String FRAGMENT_WEBVIEW = "FRAGMENT_WEBVIEW";
    public static  final String FRAGMENT_ACERCADE = "FRAGMENT_ACERCADE";
    public static  final String CURRENT_FRAGMENT = "CURRENT_FRAGMENT";


    public static  final String CURRENT_URL_WEBVIEW = "CURRENT_URL_WEBVIEW";
    public static  final String CURRENT_TITULO_WEBVIEW = "CURRENT_TITULO_WEBVIEW";

}
