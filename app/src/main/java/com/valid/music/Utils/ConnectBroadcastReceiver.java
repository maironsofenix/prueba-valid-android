package com.valid.music.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


import com.valid.music.Aplication.App;
import timber.log.Timber;

public class ConnectBroadcastReceiver extends BroadcastReceiver {

    private boolean conexionOk;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!ConnectivityStatus.isConnected(context)) {
            // no connection
            App.getInstance().getRepository().isConected = false;
            conexionOk = false;
            Timber.e("DESCONECTADO DE LA RED");
        } else {
            // connected
            App.getInstance().getRepository().isConected = true;
            conexionOk = true;
            Timber.e("CONECTADO A LA RED");
        }
    }

    public boolean isConexionOk() {
        return conexionOk;
    }
}
