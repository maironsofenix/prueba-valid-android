package com.valid.music.Aplication;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by JaimeBallesta on 23/10/17.
 */

@Module
public class Context_Module {

    private Context context;

    public Context_Module(Context context) {
        this.context = context;
    }

    @Provides
    public Context context(){
        return this.context;
    }

}
