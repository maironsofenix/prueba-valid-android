package com.valid.music.Aplication;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.valid.music.Repository.Repository;

import timber.log.Timber;

/**
 * Created by JaimeBallesta on 11/10/17.
 */

public class App extends Application {

    private static App instancie;
    private App_Component component;
    private Repository repository;
    private Context context_;
    private Activity activityClass;

    public static synchronized App getInstance() {
        if (instancie == null) {
            instancie = new App();
        }
        return instancie;
    }

    public void setContext(Context context_) {
        this.context_ = context_;
        instancie.onCreate();
    }

    @Override
    public void onCreate() {

        super.onCreate();
        instancie = this;
        Timber.plant(new Timber.DebugTree());
        //JodaTimeAndroid.init(this);

        component = DaggerApp_Component.builder()
                .context_Module(new Context_Module(context_))
                .build();
        repository = component.activateRoutesRepository();

    }

    public App_Component getComponent() {
        return component;
    }

    public Repository getRepository() {
        return repository;
    }

    public Activity getActivityClass() {
        return activityClass;
    }

    public void setActivityClass(Activity activityClass) {
        this.activityClass = activityClass;
    }
}
