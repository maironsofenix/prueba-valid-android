package com.valid.music.Aplication;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by JaimeBallesta on 23/10/17.
 */

@Scope
@Retention(RetentionPolicy.CLASS)
public @interface App_Scope
{

}
