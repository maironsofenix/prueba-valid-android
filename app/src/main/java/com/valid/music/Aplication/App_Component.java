package com.valid.music.Aplication;

import com.valid.music.Repository.Repository;
import com.valid.music.Repository.Repository_Module;
import dagger.Component;

@App_Scope
@Component(modules = Repository_Module.class)
public interface App_Component {

    Repository activateRoutesRepository();
}
